<?php

class Atom implements Iterator{
	private $content = array();
	public $title = '';
	public $link = '';
	private $position = 0;

	public function __construct($doc) {

		// Processing feed

		$y = array();
		$tnl = $doc->getElementsByTagName("title");
		$tnl = $tnl->item(0);
		$this->title = $tnl->firstChild->textContent;

		$tnl = $doc->getElementsByTagName("link");
		$tnl = $tnl->item(0);
		$this->link = $tnl->getAttribute("href");

		// Processing articles

		$entries = $doc->getElementsByTagName("entry");
		foreach ($entries as $entry) {
			// get description of article, type 1
			$this->tags($entry, 1);
		}
	}

	private function tags($item, $type) {
		$y = array();

		$y['title'] = self::getTagContent($item, 'title');
		$y['link'] = self::getTagAttribute($item, 'link', 'href');
		$y['content'] = self::getTagContent($item, 'content');
		$y['updated'] = self::getTagContent($item, 'updated');
		$y['authorName'] = self::getTagContent($item->getElementsByTagName('author')->item(0), 'name');
		$y['authorUri'] = self::getTagContent($item->getElementsByTagName('author')->item(0), 'uri');

		$this->content[] = $y;
	}

	private static function getTagContent($item, $name) {
		$tnl = $item->getElementsByTagName($name)->item(0);
		return $tnl->firstChild->textContent;
	}

	private static function getTagAttribute($item, $name, $attr) {
		$tnl = $item->getElementsByTagName($name)->item(0);
		return $tnl->getAttribute($attr);
	}

	public function rewind() {
		$this->position = 0;
	}

	public function current() {
		return $this->content[$this->position];
	}

	public function key() {
		return $this->position;
	}

	public function next() {
		++$this->position;
	}

	public function valid() {
		return isset($this->content[$this->position]);
	}
}
