<?php

class XMLCacheReader extends DOMDocument{
	/**
	 * Time in seconds
	 */
	const CACHE_TIME = 3600;

	/**
	 * Url of the xml file
	 */
	private $url;

	public function __construct($url) {
		$this->url = $url;

		$content = $this->getFile();
		parent::loadXML($content);
	}

	public function getFile() {
		if ($this->isCached()) {
			return $this->getCache();
		} else {
			return $this->getFromNet();
		}
	}

	private function getCachePath() {
		return __DIR__.'/cache/'.md5($this->url);
	}

	private function isCached() {
		return
			is_file($this->getCachePath()) &&
			filemtime($this->getCachePath()) > time() - self::CACHE_TIME;
	}

	private function getCache() {
		return file_get_contents($this->getCachePath());
	}

	private function getFromNet() {
		$cache = file_get_contents($this->url);
		file_put_contents($this->getCachePath(), $cache);
		return $cache;
	}
}
