<?php


/**
 * Config is self-explanatory
 */
$anonymIssueConfig = array(
	'repositoryUser' => 'testaccount99',
	'repositoryName' => 'testaccount99',
	'issueUser' => 'testaccount99',
	'issueToken' => 'dbd0251e98bf3596c14736c3c9299a6d',
	'pathToLib' => __DIR__.'/libs/Github/Autoloader.php',
	'salt' => 'anonymIssue',
	'defaultName' => 'Anonymous',
	'defaultEmail' => null,
);


// set autoloader
require_once($anonymIssueConfig['pathToLib']);
Github_Autoloader::register();

require_once('libs/markdown/markdown.php');

// create new instance of github api client
$github = new Github_Issue(
	$anonymIssueConfig['repositoryUser'],
	$anonymIssueConfig['repositoryName'],
	$anonymIssueConfig['issueUser'],
	$anonymIssueConfig['issueToken']
);

if (!isset($_GET['t'])) {
	$_GET['t'] = '';
}


if ($_GET['t'] == 'newIssue') {

	if (check()
	&& isset($_POST[$anonymIssueConfig['salt'].'_title'])
	&& !empty($_POST[$anonymIssueConfig['salt'].'_title'])) {

		$_title = $_POST[$anonymIssueConfig['salt'].'_title'];
		$_body = $_POST[$anonymIssueConfig['salt'].'_body'];

		$_name = $anonymIssueConfig['defaultName'];
		$_email = $anonymIssueConfig['defaultEmail'];

		if (isset($_POST[$anonymIssueConfig['salt'].'_name'])
		&& !empty($_POST[$anonymIssueConfig['salt'].'_name'])) {
			$_name = $_POST[$anonymIssueConfig['salt'].'_name'];
		}
		if (isset($_POST[$anonymIssueConfig['salt'].'_email'])
		&& !empty($_POST[$anonymIssueConfig['salt'].'_email'])) {
			$_email = $_POST[$anonymIssueConfig['salt'].'_email'];
		}

		if (!isset($_POST['insert'])) {
			$_preview = Markdown($_body);
		} else {

			// set informations about the new issue
			$github->setName($_name);
			$github->setEmail($_email);

			// add issues
			$number = $github->sendIssue($_title, $_body);

			// get this page without POST
			$url = 'http://';
			if ($_SERVER['HTTPS'] == 'on') {
				$url = 'https://';
			}
			$url .=
				$_SERVER['HTTP_HOST'].
				$_SERVER['REQUEST_URI'].
				'?'.http_build_query(array(
					'page' => $_GET['page'],
					'number' => $number,
					't' => 'show'
				));

			header('Location: '.$url);
			exit();
		}

	} else {
		$_title = '';
		$_name = '';
		$_email = '';
		$_body = '';
	}

	// generate form
	echo '<form method="post" action="">'.
		'<p class="anonymIssue">'.
			'<label for="'.$anonymIssueConfig['salt'].'_name">Name</label>'.
			'<input type="text" value="',$_name,'" name="'.$anonymIssueConfig['salt'].'_name" id="'.$anonymIssueConfig['salt'].'_name"/><br class="clear"/>'.
			'<label for="'.$anonymIssueConfig['salt'].'_email">Email</label>'.
			'<input type="text" value="',$_email,'" name="'.$anonymIssueConfig['salt'].'_email" id="'.$anonymIssueConfig['salt'].'_email"/><br class="clear"/>'.
			'<label for="'.$anonymIssueConfig['salt'].'_title">Title ¹</label>'.
			'<input type="text" value="',$_title,'" name="'.$anonymIssueConfig['salt'].'_title" id="'.$anonymIssueConfig['salt'].'_title"/><br class="clear"/>'.
			'<input type="hidden" value="" name="'.$anonymIssueConfig['salt'].'_js" id="'.$anonymIssueConfig['salt'].'_js"/>'.
			'<label for="'.$anonymIssueConfig['salt'].'_body">Body ¹ ²</label>'.
			'<textarea rows="10" cols="60" name="'.$anonymIssueConfig['salt'].'_body" id="'.$anonymIssueConfig['salt'].'_body">',$_body,'</textarea><br class="clear"/>'.
			'<button type="submit" name="insert">Insert</button>&nbsp;<button type="submit" name="preview">Preview</button>'.
		'</p>'.
		'<ol class="hints">'.
			'<li>Required</li>'.
			'<li>Markdown - <a href="http://daringfireball.net/projects/markdown/syntax">Syntax</a>, <a href="http://github.github.com/github-flavored-markdown/">Github-Syntax</a></li>'.
		'</ol>'.
	'</form>'.
	'<script type="text/javascript">'."\n".
		'/* <!-- */'."\n".
		'document.getElementById("'.$anonymIssueConfig['salt'].'_js").value = (new Date).getFullYear() + "." + ((new Date).getMonth()+1);'."\n".
		'/* --> */'."\n".
	'</script>'."\n";

	if (isset($_preview)) {
		echo '<h3>Preview</h3>',
		'<div class="markdown">',$_preview,'</div>';
	}


} elseif ($_GET['t'] == 'listOpen' || $_GET['t'] == 'listClosed') {

	if ($_GET['t'] == 'listClosed') {
		echo '<h1>Closed</h1>';
		$list = $github->getIssues('closed');
	} elseif ($_GET['t'] == 'listOpen') {
		echo '<h1>Open</h1>';
		$list = $github->getIssues('open');
	}

	echo '<div class="tickets">';
	foreach ($list as $issue) {
		echo '<div class="ticket">';
		echo '<div class="date">',strftime('%x %X', strtotime($issue['created_at'])),' von '.getAnonymUser($issue['body'], $issue['user']).'</div>';
		echo '<a title="Original content" class="link" href="',$issue['html_url'],'"></a>';
		echo '<div class="title"><a href="?page=tickets&amp;t=show&amp;number='.$issue['number'].'">'.$issue['title'].'</a><span>',$issue['comments'],' comments</span></div>';
		echo '</div>';
	}
	echo '</div>';
	echo '<p><a href="?page=tickets&amp;t=newIssue">Create new</a></p>';


} elseif ($_GET['t'] == 'show' && isset($_GET['number']) && is_numeric($_GET['number'])) {

	$issue = $github->getIssue($_GET['number']);

	echo
		'<ul class="info">',
			'<li>'.getAnonymUser($issue['body'], $issue['user']).'</li>',
			'<li>',strftime('%x %X', strtotime($issue['created_at'])),'</li>',
			'<li>',$issue['comments'],' comments</li>',
			'<li><a title="Orignial content" class="link" href="',$issue['html_url'],'"></a></li>',
		'</ul>';
	echo '<h1>'.$issue['title'].'</h1>';
	echo '<div class="markdown clear">'.Markdown($issue['body']).'</div>';


	$comments = $github->getComments($_GET['number']);
	foreach ($comments as $comment) {
		echo '<div class="ticket">';
			echo '<div class="date">',strftime('%x %X', strtotime($comment['created_at'])),' von '.getAnonymUser($comment['body'], $comment['user']).'</div>';
			echo '<div class="markdown clear hr">'.Markdown($comment['body']).'</div>';
		echo '</div>';
	}

	echo '<p><a href="?page=tickets&amp;t=newComment&amp;number='.$_GET['number'].'">Add comment</a></p>';


} elseif ($_GET['t'] == 'newComment' && isset($_GET['number']) && is_numeric($_GET['number'])) {

	if (check()) {

		$_body = $_POST[$anonymIssueConfig['salt'].'_body'];

		$_name = $anonymIssueConfig['defaultName'];
		$_email = $anonymIssueConfig['defaultEmail'];

		if (isset($_POST[$anonymIssueConfig['salt'].'_name'])
		&& !empty($_POST[$anonymIssueConfig['salt'].'_name'])) {
			$_name = $_POST[$anonymIssueConfig['salt'].'_name'];
		}
		if (isset($_POST[$anonymIssueConfig['salt'].'_email'])
		&& !empty($_POST[$anonymIssueConfig['salt'].'_email'])) {
			$_email = $_POST[$anonymIssueConfig['salt'].'_email'];
		}

		if (!isset($_POST['insert'])) {
			$_preview = Markdown($_body);
		} else {

			// set informations about the new comment
			$github->setName($_name);
			$github->setEmail($_email);

			// add comment
			$number = $github->sendComment($_GET['number'], $_body);

			header('Location: ?page=tickets&t=show&number='.$_GET['number']);
			exit();
		}

	} else {

		$_name = '';
		$_email = '';
		$_body = '';
	}

	// generate form
	echo '<form method="post" action="">'.
		'<p class="anonymIssue">'.
			'<label for="'.$anonymIssueConfig['salt'].'_name">Name</label>'.
			'<input type="text" value="',$_name,'" name="'.$anonymIssueConfig['salt'].'_name" id="'.$anonymIssueConfig['salt'].'_name"/><br class="clear"/>'.
			'<label for="'.$anonymIssueConfig['salt'].'_email">Email</label>'.
			'<input type="text" value="',$_email,'" name="'.$anonymIssueConfig['salt'].'_email" id="'.$anonymIssueConfig['salt'].'_email"/><br class="clear"/>'.
			'<input type="hidden" value="" name="'.$anonymIssueConfig['salt'].'_js" id="'.$anonymIssueConfig['salt'].'_js"/>'.
			'<label for="'.$anonymIssueConfig['salt'].'_body">Body¹²</label>'.
			'<textarea rows="10" cols="60" name="'.$anonymIssueConfig['salt'].'_body" id="'.$anonymIssueConfig['salt'].'_body">',$_body,'</textarea><br class="clear"/>'.
			'<button type="submit" name="insert">Add comment</button>&nbsp;<button type="submit" name="preview">Preview</button>',
		'</p>'.
		'<ol class="hints">'.
			'<li>Required</li>'.
		'<li>Markdown - <a href="http://daringfireball.net/projects/markdown/syntax">Syntax</a>, <a href="http://github.github.com/github-flavored-markdown/">Github-Syntax</a></li>'.
		'</ol>'.
	'</form>'.
	'<script type="text/javascript">'."\n".
		'/* <!-- */'."\n".
		'document.getElementById("'.$anonymIssueConfig['salt'].'_js").value = (new Date).getFullYear() + "." + ((new Date).getMonth()+1);'."\n".
		'/* --> */'."\n".
	'</script>'."\n";

	if (isset($_preview)) {
		echo '<h3>Preview</h3>',
		'<div class="markdown">',$_preview,'</div>';
	}




} else {

?>
<h1>Issues</h1>
<ul>
	<li><a href="?page=tickets&amp;t=listOpen">Open</a></li>
	<li><a href="?page=tickets&amp;t=listClosed">Closed</a></li>
</ul>
<p><a href="?page=tickets&amp;t=newIssue">New issue</a></p>

<?php
}



function check_spam() {
	global $anonymIssueConfig;
	return (isset($_POST[$anonymIssueConfig['salt'].'_js']) && $_POST[$anonymIssueConfig['salt'].'_js'] == date('Y.n'));
}

function check() {
	global $anonymIssueConfig;
	return (check_spam() && isset($_POST[$anonymIssueConfig['salt'].'_body']) && !empty($_POST[$anonymIssueConfig['salt'].'_body']));
}

function getAnonymUser($body, $user) {

	if (preg_match('|^From: \*\*(.+)\*\*(\(\*.+\*\))?|', $body, $result)) {
		return
			$result[1].
			' <span class="real-user">(<a href="https://github.com/'.$user.'/">'.$user.'</a>)</span>';
	} else {
		return '<a href="https://github.com/'.$user.'/">'.$user.'</a>';
	}
}

