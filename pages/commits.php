<?php
//https://github.com/BKampfBot/BKampfBot/commits/master.atom

include_once('libs/XMLCacheReader/XMLCacheReader.php');
include_once('libs/atom.php');
$Parser = new XmlCacheReader('https://github.com/georf/anonymIssue/commits/master.atom');

$atom = new Atom($Parser);

echo '<div class="tickets">';
foreach ($atom as $commit) {
	echo '<div class="ticket">';
	echo '<div class="date">',strftime('%x %X', strtotime($commit['updated'])),' von <a href="',$commit['authorUri'],'">',$commit['authorName'],'</a></div>';
	echo '<a title="Orignialer Commit bei Github" class="link" href="',$commit['link'],'"></a>';
	echo '<div class="clear">',$commit['content'],'</div>';
	echo '</div>';
}
echo '</div>';

