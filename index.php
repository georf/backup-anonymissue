<?php

error_reporting(E_ALL);
//setlocale (LC_ALL, 'de_DE.UTF8');

ob_start();

if (!isset($_GET['page'])) {
	$_GET['page'] = 'home';
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>anonymIssue</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<link href="m/s.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="header"><?php
$pages = array(
	'home' => array(
		'name' => 'Home',
		'title' => 'Home',
		'file' => 'home.php',
	),
	'tickets' => array(
		'name' => 'Issues',
		'title' => 'Create and comment issues',
		'file' => 'tickets.php',
	),
	'commits' => array(
		'name' => 'Commits',
		'title' => 'View last commits',
		'file' => 'commits.php',
	),
);

foreach ($pages as $link => $object) {
	echo '<a href="?page=',$link,'"';

	if ($_GET['page'] === $link) {
		echo ' class="active"';
	}

	echo '>',$object['name'],'</a>';
}

echo '</div><div id="content">';

if (isset($pages[$_GET['page']]) && is_file('pages/'.$pages[$_GET['page']]['file'])) {
	try {

		include('pages/'.$pages[$_GET['page']]['file']);

	} catch(Exception $e) {
		echo '<div class="error">',$e->getMessage(),'</div>';
	}
} else {
	include('pages/404.php');
}

ob_end_flush();
?>
</div>
<div id="footer">
<div class="icons">
<a title="XHTML 1 valide" href="http://validator.w3.org/check?uri=referer"><img src="m/valid-xhtml10.png" alt="" height="31" width="88"/></a>
<a title="CSS 3 valide" href="http://jigsaw.w3.org/css-validator/check/referer"><img width="88" height="31" src="m/vcss.gif" alt=""/></a>
<a title="CC-BY-NC-SA" href="http://creativecommons.org/licenses/by-nc-sa/2.0/de/"><img width="88" height="31" src="m/ccbyncsa.png" alt=""/></a>
<a title="Entwicklerblog" href="http://www.georf.de/"><img width="88" height="31" src="m/georf.png" alt=""/></a>
</div>
</div>
</body>
</html>
